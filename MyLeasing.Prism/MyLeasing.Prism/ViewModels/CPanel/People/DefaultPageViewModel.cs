﻿using MyLeasing.Common.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyLeasing.Prism.ViewModels.CPanel.People
{
    public class DefaultPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private string _iconImagePage;

        public ObservableCollection<Person> People { get; private set; }
        public ICommand OutputAgeCommand { get; private set; }
        public string SelectedItemText { get; private set; }

        public string IconImagePage
        {
            get => _iconImagePage;
            set => SetProperty(ref _iconImagePage, value);
        }


        public DefaultPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            this._navigationService = navigationService;
            Title = "People";
            People = new ObservableCollection<Person>
            {
                new Person ("Steve", 21),
                new Person ("John", 37),
                new Person ("Tom", 42),
                new Person ("Lucas", 29),
                new Person ("Tariq", 39),
                new Person ("Jane", 30)
            };
            OutputAgeCommand = new Command<Person>(OutputAge);
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    IconImagePage = "ic_tb_action_people";
                    break;
                case Device.Android:
                    IconImagePage = "";
                    break;
                case Device.UWP:
                default:
                    IconImagePage = "ic_tb_action_people";
                    break;
            }
        }


        private async void OutputAge(Person person)
        {
            //Ir a la pagina del detalle de la persona
            var parameter = new NavigationParameters();
            parameter.Add("person", person);
            await _navigationService.NavigateAsync("CPanelPeopleQuickViewPage", parameter);
        }
    }
}
