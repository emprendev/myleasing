﻿using MyLeasing.Common.Helpers;
using MyLeasing.Common.Models;
using Prism.Commands;
using Prism.Navigation;


namespace MyLeasing.Prism.ViewModels.CPanel
{
    public class MenuItemViewModel : Menu
    {
        private readonly INavigationService _navigationService;
        private DelegateCommand _selectMenuCommand;

        public MenuItemViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public DelegateCommand SelectMenuCommand => _selectMenuCommand ?? (_selectMenuCommand = new DelegateCommand(SelectMenu));

        private async void SelectMenu()
        {
            if (PageName.Equals("SignInPage"))
            {
                Settings.IsRemember = false;
                await _navigationService.NavigateAsync("/NavigationPage/SignInPage");
                return;
            }
            await _navigationService.NavigateAsync($"/CPanelLeasingMasterDetailPage/NavigationPage/{PageName}");
        }
    }

}
