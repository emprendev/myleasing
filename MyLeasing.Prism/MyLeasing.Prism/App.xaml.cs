﻿using Prism;
using Prism.Ioc;
using MyLeasing.Prism.ViewModels;
using MyLeasing.Prism.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MyLeasing.Common.Services;
using Newtonsoft.Json;
using MyLeasing.Common.Models;
using MyLeasing.Common.Helpers;
using System;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MyLeasing.Prism
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTQzOTM4QDMxMzcyZTMyMmUzMENQTGoxbm4rWTdnQ2JJcHlRajVFZnoyK3k3enB1NXM0OVREZ01veUY5aWs9");

            InitializeComponent();

            var token = JsonConvert.DeserializeObject<TokenResponse>(Settings.Token);
            if (Settings.IsRemember && token?.Expiration > DateTime.Now)
            {
                await NavigationService.NavigateAsync("/CPanelLeasingMasterDetailPage/NavigationPage/CPanelPropertiesDefaultPage");
            }
            else
            {
                await NavigationService.NavigateAsync("NavigationPage/SignInPage");
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.Register<IApiService, ApiService>();

            // Pages
            containerRegistry.RegisterForNavigation<NavigationPage>();
            // Pagina de inicio APP
            containerRegistry.RegisterForNavigation<SignInPage, SignInPageViewModel>();
            //Pagina de menu (MasterDetailPage)
            containerRegistry.RegisterForNavigation<Views.CPanel.LeasingMasterDetailPage, ViewModels.CPanel.LeasingMasterDetailPageViewModel>("CPanelLeasingMasterDetailPage");
            //Properties Pages
            containerRegistry.RegisterForNavigation<Views.CPanel.Properties.DefaultPage, ViewModels.CPanel.Properties.DefaultPageViewModel>("CPanelPropertiesDefaultPage");
            containerRegistry.RegisterForNavigation<Views.CPanel.Properties.QuickViewPage, ViewModels.CPanel.Properties.QuickViewPageViewModel>("CPanelPropertiesQuickViewPage");
            containerRegistry.RegisterForNavigation<Views.CPanel.Properties.PropertyTabbedPage, ViewModels.CPanel.Properties.PropertyTabbedPageViewModel>("CPanelPropertiesPropertyTabbedPage");
            //People Pages
            containerRegistry.RegisterForNavigation<Views.CPanel.People.DefaultPage, ViewModels.CPanel.People.DefaultPageViewModel>("CPanelPeopleDefaultPage");
            containerRegistry.RegisterForNavigation<Views.CPanel.People.QuickViewPage, ViewModels.CPanel.People.QuickViewPageViewModel>("CPanelPeopleQuickViewPage");
            //Contracts Pages
            containerRegistry.RegisterForNavigation<Views.CPanel.Contracts.DefaultPage, ViewModels.CPanel.Contracts.DefaultPageViewModel>("CPanelContractsDefaultPage");
            containerRegistry.RegisterForNavigation<Views.CPanel.Contracts.QuickViewPage, ViewModels.CPanel.Contracts.QuickViewPageViewModel>("CPanelContractsQuickViewPage");
            // UserPages
            containerRegistry.RegisterForNavigation <Views.CPanel.Users.EditPage, ViewModels.CPanel.Users.EditPageViewModel>("CPanelUsersEditPage");
            containerRegistry.RegisterForNavigation<Views.CPanel.Users.MapPage, ViewModels.CPanel.Users.MapPageViewModel>("CPanelUsersMapPage");
           
        }
    }
}
