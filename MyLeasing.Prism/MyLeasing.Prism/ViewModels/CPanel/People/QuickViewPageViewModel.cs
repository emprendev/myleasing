﻿using MyLeasing.Common.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyLeasing.Prism.ViewModels.CPanel.People
{
    public class QuickViewPageViewModel : ViewModelBase
    {
        private Person _person;
        public Person Person
        {
            get => _person;
            set => SetProperty(ref _person, value);
        }

        public QuickViewPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Person Detail";
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("person"))
            {
                Person = parameters.GetValue<Person>("person");
                Title = string.Format("{0} is {1} years old.", Person.Name, Person.Age);
            }
        }
    }
}
