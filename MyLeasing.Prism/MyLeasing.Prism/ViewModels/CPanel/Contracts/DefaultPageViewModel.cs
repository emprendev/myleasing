﻿using MyLeasing.Common.Helpers;
using MyLeasing.Common.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyLeasing.Prism.ViewModels.CPanel.Contracts
{
    public class DefaultPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private PropertyResponse _property;
        private ObservableCollection<ContractResponse> _contracts;
        private string _iconImagePage;

        public PropertyResponse Property
        {
            get => _property;
            set => SetProperty(ref _property, value);
        }

        public string IconImagePage
        {
            get => _iconImagePage;
            set => SetProperty(ref _iconImagePage, value);
        }

        public ObservableCollection<ContractResponse> Contracts
        {
            get => _contracts;
            set => SetProperty(ref _contracts, value);
        }

        public ICommand SelectContractCommand { get; private set; }

        public DefaultPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            this._navigationService = navigationService;
            Title = "Contracts";
            Property = JsonConvert.DeserializeObject<PropertyResponse>(Settings.Property);
            LoadContracts();
            SelectContractCommand = new Command<ContractResponse>(SelectContract);
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    IconImagePage = "ic_tb_action_list_alt";
                    break;
                case Device.Android:
                    IconImagePage = "";
                    break;
                case Device.UWP:
                default:
                    IconImagePage = "ic_tb_action_list_alt";
                    break;
            }
        }

        private void LoadContracts()
        {
            Contracts = new ObservableCollection<ContractResponse>(Property.Contracts);
        }
       
        private async void SelectContract(ContractResponse contract)
        {
            //Ir a la pagina del detalle del detalle del contrato
            var parameter = new NavigationParameters();
            parameter.Add("contract", contract);
            await _navigationService.NavigateAsync("CPanelContractsQuickViewPage", parameter);
        }

    }
}
