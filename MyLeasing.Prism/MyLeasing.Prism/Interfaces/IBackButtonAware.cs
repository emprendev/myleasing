﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLeasing.Prism.Interfaces
{
    public interface IBackButtonAware
    {
        bool OnBackButtonPressed();
    }
}
