﻿using MyLeasing.Common.Helpers;
using MyLeasing.Common.Models;
using MyLeasing.Prism.Interfaces;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace MyLeasing.Prism.ViewModels.CPanel.Properties
{
    public class DefaultPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private OwnerResponse _owner;
        
        private ObservableCollection<PropertyResponse> _properties;
        public ObservableCollection<PropertyResponse> Properties
        {
            get => _properties;
            set => SetProperty(ref _properties, value);
        }


        public ICommand SelectPropertyBehaviorCommand { get; private set; }
        public DefaultPageViewModel(INavigationService navigationService):base(navigationService)
        {
            this._navigationService = navigationService;
            LoadOwner();
            Title = "Properties";
            SelectPropertyBehaviorCommand = new Command<PropertyResponse>(SelectProperty);
        }
        private async void SelectProperty(PropertyResponse Property)
        {
            
            // -- Al ver el detalle de la propiedad , navegamos a una pagina tipo Tabbed
            // -- donde tenemos los detalles de la propiedad y los contratos de la misma
            Settings.Property = JsonConvert.SerializeObject(Property);
            await _navigationService.NavigateAsync("CPanelPropertiesPropertyTabbedPage");
        }

        //public override void OnNavigatedTo(INavigationParameters parameters)
        //{
        //    base.OnNavigatedTo(parameters);
            
        //    //if (parameters.ContainsKey("owner"))
        //    //{
        //    //    _owner = parameters.GetValue<OwnerResponse>("owner");
        //    //    Title = $"Properties of: {_owner.FullName}";
        //    //    //Listado de propiedades
        //    //    Properties = new ObservableCollection<PropertyResponse>(_owner.Properties);
        //    //}
        //}

        private void LoadOwner()
        {
            _owner = JsonConvert.DeserializeObject<OwnerResponse>(Settings.Owner);
            Title = $"Properties of: {_owner.FullName}";
            //Listado de propiedades
            Properties = new ObservableCollection<PropertyResponse>(_owner.Properties);
        }
    }
}
