﻿using MyLeasing.Common.Helpers;
using MyLeasing.Common.Models;
using Newtonsoft.Json;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace MyLeasing.Prism.ViewModels.CPanel.Properties
{
    public class QuickViewPageViewModel : ViewModelBase
    {
        private PropertyResponse _property;
        private ObservableCollection<RotatorModel> _imageCollection;
        private string _iconImagePage;

        public PropertyResponse Property
        {
            get => _property;
            set => SetProperty(ref _property, value);
        }

        public string IconImagePage
        {
            get => _iconImagePage;
            set => SetProperty(ref _iconImagePage, value);
        }
        public QuickViewPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Details";
            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    IconImagePage = "ic_tb_action_home";
                    break;
                case Device.Android:
                    IconImagePage = "";
                    break;
                case Device.UWP:
                default:
                    IconImagePage = "ic_tb_action_home";
                    break;
            }
        }

        public ObservableCollection<RotatorModel> ImageCollection
        {
            get => _imageCollection;
            set => SetProperty(ref _imageCollection, value);
        }
        private void LoadImages()
        {
            ImageCollection = new ObservableCollection<RotatorModel>(Property.PropertyImages.Select(pi => new RotatorModel
            {
                Image = pi.ImageUrl
            }).ToList());
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            Property = JsonConvert.DeserializeObject<PropertyResponse>(Settings.Property);
            LoadImages();
        }
    }
}

