﻿using MyLeasing.Common.Helpers;
using MyLeasing.Common.Models;
using MyLeasing.Common.Services;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Navigation;
using System;

namespace MyLeasing.Prism.ViewModels
{
    public class SignInPageViewModel : ViewModelBase
    {
        private readonly INavigationService _navigationService;
        private readonly IApiService _apiService;
        private string _email;
        private string _password;
        private bool _isRemember;
        private bool _isRunning;
        private bool _isEnabled;
        private DelegateCommand _loginCommand;

        public SignInPageViewModel(
            INavigationService navigationService,
            IApiService apiService) : base(navigationService)
        {
            Title = "SignIn";
            IsEnabled = true;
            IsRemember = true;
            this._navigationService = navigationService;
            this._apiService = apiService;
            Email = "jzuluaga55@hotmail.com";
            Password = "123456";
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public bool IsRemember
        {
            get => _isRemember;
            set => SetProperty(ref _isRemember, value);
        }

        public bool IsRunning
        {
            get => _isRunning;
            set => SetProperty(ref _isRunning, value);
        }

        public bool IsEnabled
        {
            get => _isEnabled;
            set => SetProperty(ref _isEnabled, value);
        }

        public DelegateCommand LoginCommand => _loginCommand ?? (_loginCommand = new DelegateCommand(Login));

        private async void Login()
        {
            if (string.IsNullOrEmpty(Email))
            {
                await App.Current.MainPage.DisplayAlert("Error", "You must enter a email", "Accept");
                return;
            }

            if (string.IsNullOrEmpty(Password))
            {
                await App.Current.MainPage.DisplayAlert("Error", "You must enter a password", "Accept");
                return;
            }
            
            IsRunning = true;
            IsEnabled = false;

            var url = App.Current.Resources["UrlAPI"].ToString();
            var connection = await _apiService.CheckConnectionAsync(url);

            if (!connection)
            {
                IsRunning = false;
                IsEnabled = true;
                await App.Current.MainPage.DisplayAlert("Error", "Check the internet connection", "Accept");
                return;
            }

            //Consumir servicio de Login
            var request = new TokenRequest
            {
                Username = Email,
                Password = Password
            };


            var response = await _apiService.GetTokenAsync(url, "/Account", "/CreateToken", request);
            if (!response.IsSuccess)
            {
                await App.Current.MainPage.DisplayAlert("Error", "User or Password incorrect", "Accept");
                Password = string.Empty;
                IsRunning = false;
                IsEnabled = true;
                return;
            }

            var token = response.Result;
            var response2 = await _apiService.GetOwnerByEmailAsync(
                url,
                "api",
                "/Owners/GetOwnerByEmail",
                "bearer",
                token.Token,
                Email
                );

            if (!response2.IsSuccess)
            {
                IsRunning = false;
                IsEnabled = true;
                await App.Current.MainPage.DisplayAlert("Error", "Problem with user data. Call the administrator", "Accept");
                return;
            }

            var owner = response2.Result;
            // Guardar en persistencia el Owner y Token
            Settings.Owner = JsonConvert.SerializeObject(owner);
            Settings.Token = JsonConvert.SerializeObject(token);
            Settings.IsRemember = IsRemember;

            //var parameters = new NavigationParameters();
            //parameters.Add("owner", owner);

            IsRunning = false;
            IsEnabled = true;

            await _navigationService.NavigateAsync("/CPanelLeasingMasterDetailPage/NavigationPage/CPanelPropertiesDefaultPage");
            //await _navigationService.NavigateAsync("CPanelPeopleDefaultPage");
        }
    }
}