﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyLeasing.Common.Models
{
    public class Person
    {
        public string Name { get; private set; }

        public int Age { get; private set; }

        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}
