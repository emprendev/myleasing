﻿using MyLeasing.Common.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyLeasing.Prism.ViewModels.CPanel.Contracts
{
    public class QuickViewPageViewModel : ViewModelBase
    {
        private ContractResponse _contract;
        public ContractResponse Contract
        {
            get => _contract;
            set => SetProperty(ref _contract, value);
        }

        public QuickViewPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            Title = "Contract Detail";
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            base.OnNavigatedTo(parameters);
            if (parameters.ContainsKey("contract"))
            {
                Contract = parameters.GetValue<ContractResponse>("contract");
                Title = string.Format("Contract of: {0}", Contract.Lessee.FullName);
            }
        }
    }
}
