﻿using Xamarin.Forms;

namespace MyLeasing.Prism.Views.CPanel.Properties
{
    public partial class DefaultPage : ContentPage
    {
        public DefaultPage()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            Device.BeginInvokeOnMainThread(async () => {
                var result = await this.DisplayAlert("Alert!", "Do you really want to exit?", "Yes", "No");
                if (result) await this.Navigation.PopAsync(); // or anything else
            });
            return true;
        }
    }
}
