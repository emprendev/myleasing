﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyLeasing.Prism.ViewModels.CPanel.Users
{
    public class EditPageViewModel : ViewModelBase
    {
        public EditPageViewModel(INavigationService navigationService):base(navigationService)
        {
            Title = "Modify User";
        }
    }
}
